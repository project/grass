<?php
/**
* Implementation of THEMEHOOK_settings() function.
*
* @param $saved_settings
*   array An array of saved settings for this theme.
* @return
*   array A form array.
*/
function phptemplate_settings($saved_settings) {
	$defaults = array(
		'grass_width' => '1024px',
		);

  // Create the form widgets using Forms API
  $form['grass_width'] = array(
    '#type' => 'select',
    '#title' => t('Site width'),
    '#default_value' => $settings['grass_width'],
		'#options' => array(
			'960px' => t('Fixed 960px'),
			'1024px' => t('Fixed 1024px'),
			'70%' => t('Fluid 70%'),
			'80%' => t('Fluid 80%'),
			'90%' => t('Fluid 90%'),
		  ),
    '#description' => t('select a width for the site'),
  );

  // Return the additional form widgets
  return $form;
}
