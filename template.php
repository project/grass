<?php

/**
 * Implementation of hook_theme().
 *
 * @return
 */
function grass_theme() {
  return array(
    // Custom theme functions.
    'search_block_form' => array(
      'arguments' => array('form' => NULL),
    ),
    'breadcrumb' => array(
      'arguments' => array('breadcrumb' => array()),
    ),
    'fieldset' => array(),
    'id_safe' => array(
      'arguments' => array('string'),
      ),
    'conditional_stylesheets' => array(),
    'render_attributes' => array(
      'arguments' => array('attributes'),
    )
  );
}
/*
* Initialize theme settings
*/
if (is_null(theme_get_setting('grass_width'))) {
  global $theme_key;

  /*
   * The default values for the theme variables. Make sure $defaults exactly
   * matches the $defaults in the theme-settings.php file.
   */
  $defaults = array(             // <-- change this array
    'grass_width' => '102spx',
  );

  // Get default theme settings.
  $settings = theme_get_settings($theme_key);
  // Don't save the toggle_node_info_ variables.
  if (module_exists('node')) {
    foreach (node_get_types() as $type => $name) {
      unset($settings['toggle_node_info_' . $type]);
    }
  }
  // Save default theme settings.
  variable_set(
    str_replace('/', '_', 'theme_'. $theme_key .'_settings'),
    array_merge($defaults, $settings)
  );
  // Force refresh of Drupal internals.
  theme_get_setting('', TRUE);
}
/**
 * Add Superfish JS file if it exists
 * 
 */
$superfish = drupal_get_path('theme', 'grass') .'/js/superfish.js';
if (file_exists ($superfish)) {
  drupal_add_js($superfish);
}

/**
 * Theme override for search block form.
 *
 * Remove the title from output.
 */
function grass_search_block_form($form) {
  $output = '';
  //$output .= dsm($form);

  unset($form['search_block_form']['#title']);

  $output .= drupal_render($form);
  return $output;
}

/**
  * Implementation of theme_breadcrumb()
  * @see theme_breadcrumb(), grass_theme();
  *
  * Changed breadcrumb separator to an image and add current page's title to end
  */
function grass_breadcrumb($breadcrumb) {
  if (!empty($breadcrumb)) {
    $breadcrumb[] = drupal_get_title();
    $separator = '>';
    return '<div class="breadcrumb">'. implode(' '. $separator .' ', $breadcrumb) .'</div>';
  }
}

/**
  * Implementation of theme_fieldset
  *
  * @see theme_fieldset(), grass_theme(), grass.js
  *
  * Added a class of fcollapsed to help with styling.
  */
function grass_fieldset($element) {
  if (!empty($element['#collapsible'])) {

    if (!isset($element['#attributes']['class'])) {
      $element['#attributes']['class'] = '';
    }

    $element['#attributes']['class'] .= ' collapsible';
    if (!empty($element['#collapsed'])) {
      $element['#attributes']['class'] .= ' collapsed';
    }
  }

  return '<fieldset'. drupal_attributes($element['#attributes']) .'>'. ($element['#title'] ? '<div class="fieldset-title">'. $element['#title'] .'</div>' : '') .'<div class="fieldset-body">'. (isset($element['#description']) && $element['#description'] ? '<div class="description">'. $element['#description'] .'</div>' : '') . (!empty($element['#children']) ? $element['#children'] : '') . (isset($element['#value']) ? $element['#value'] : '') ."</div> </fieldset>\n";
}

/**
 * CSS Filter
 * Borrowed from Studio ;)
 */
function grass_id_safe($string) {
  // Replace with dashes anything that isn't A-Z, numbers, dashes, or underscores.
  $string = drupal_strtolower(preg_replace('/[^a-zA-Z0-9-]+/', '-', $string));
  // If the first character is not a-z, add 'n' in front.
  if (!ctype_lower($string{0})) { // Don't use ctype_alpha since its locale aware.
    $string = 'id'. $string;
  }
  return $string;
}

/**
 * Conditional Stylesheets
 * Loads alternate stylesheets for Internet Explorer
 */
function grass_conditional_stylesheets() {
  // Targets IE 6 and under
  $output = "\n".'<!--[if lt IE 7.0]><link rel="stylesheet" href="'. base_path() . path_to_theme() .'/css/ie-6.css" type="text/css" media="all" charset="utf-8" /><![endif]-->'."\n";
  // Targets IE 7
  $output .= '<!--[if IE 7.0]><link rel="stylesheet" href="'. base_path() . path_to_theme() .'/css/ie-7.css" type="text/css" media="all" charset="utf-8" /><![endif]-->'."\n";
  return $output;
  $output .= '<meta equiv="X-UA-Compatible" content="IE=8">';
}

/**
 * Create a string of attributes form a provided array.
 * Borrowed from Studio, http://drupal.org/project/studio
 *
 * @param $attributes
 * @return string
 */
function grass_render_attributes($attributes) {
  if ($attributes) {
    $items = array();
    foreach ($attributes as $attribute => $data) {
      if (is_array($data)) {
        $data = implode(' ', $data);
      }
      $items[] = $attribute .'="'. $data .'"';
    }
    $output = ' '. str_replace('_', '-', implode(' ', $items));
  }
  return $output;
}

/**
 * Implementation of hook_preprocess()
 * 
 * This function checks to see if a hook has a preprocess file associated with 
 * it, and if so, loads it.
 * 
 * @param $vars
 * @param $hook
 * @return Array
 */
function grass_preprocess(&$vars, $hook) {
  if(is_file(drupal_get_path('theme', 'grass') . '/preprocess/preprocess-' . str_replace('_', '-', $hook) . '.inc')) {
    include('preprocess/preprocess-' . str_replace('_', '-', $hook) . '.inc');
  }
}

/**
 * Implementation of hook_preprocess.
 *
 * Using a function in template.php instead of a separate file
 */
function grass_preprocess_node_content(&$vars) {
  //Create alternate template file name for the specific node types.
  $vars['template_files'][] = 'node-content-' . str_replace('_', '-', $vars['node']->type);

  //By default, set the display to be the regular content.
   $vars['display'] = $vars['content'];
}

drupal_add_js('Cufon.replace(\'.box h3, h1.title, #sidebar h3, #feature h3, .box h2, .breadcrumb, #comments h3, .date .day\');', 'inline');
drupal_add_js('Cufon.replace(\'h1#site-name, #nav ul li a, .breadcrumb a, h2 a, .comment h4\', {hover:true});', 'inline');