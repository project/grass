<?php

/**
 * @file page.tpl.php
 *
 * Theme implementation to display a single Drupal page.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $css: An array of CSS files for the current page.
 * - $directory: The directory the theme is located in, e.g. themes/garland or
 *   themes/garland/minelli.
 * - $is_front: TRUE if the current page is the front page. Used to toggle the mission statement.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Page metadata:
 * - $language: (object) The language the site is being displayed in.
 *   $language->language contains its textual representation.
 *   $language->dir contains the language direction. It will either be 'ltr' or 'rtl'.
 * - $head_title: A modified version of the page title, for use in the TITLE tag.
 * - $head: Markup for the HEAD section (including meta tags, keyword tags, and
 *   so on).
 * - $styles: Style tags necessary to import all CSS files for the page.
 * - $scripts: Script tags necessary to load the JavaScript files and settings
 *   for the page.
 * - $body_classes: A set of CSS classes for the BODY tag. This contains flags
 *   indicating the current layout (multiple columns, single column), the current
 *   path, whether the user is logged in, and so on.
 * - $body_attributes: This is similar to $body_classes, except it goes further.
 *   There is the addition of the 'id' to the tag and more classes by default.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 * - $mission: The text of the site mission, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $search_box: HTML to display the search box, empty if search has been disabled.
 * - $primary_links (array): An array containing primary navigation links for the
 *   site, if they have been configured.
 * - $secondary_links (array): An array containing secondary navigation links for
 *   the site, if they have been configured.
 *
 * Page content (in order of occurrance in the default page.tpl.php):
 * - $left: The HTML for the left sidebar.
 *
 * - $breadcrumb: The breadcrumb trail for the current page.
 * - $title: The page title, for use in the actual HTML content.
 * - $help: Dynamic help text, mostly for admin pages.
 * - $messages: HTML for status and error messages. Should be displayed prominently.
 * - $tabs: Tabs linking to any sub-pages beneath the current page (e.g., the view
 *   and edit tabs when displaying a node).
 *
 * - $content: The main content of the current Drupal page.
 *
 * - $right: The HTML for the right sidebar.
 *
 * Footer/closing data:
 * - $feed_icons: A string of all feed icons for the current page.
 * - $footer_message: The footer message as defined in the admin settings.
 * - $footer : The footer region.
 * - $closure: Final closing markup from any modules that have altered the page.
 *   This variable should always be output last, after all other dynamic content.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language ?>" xml:lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
  <head>
    <?php print $head; ?>
    <title>
      <?php print $head_title; ?>
    </title>
    <?php print $styles; ?>
    <?php print $ie_styles; ?>
    <?php print $scripts; ?>
    <script type="text/javascript">
      //<![CDATA[
      <?php /* Needed to avoid Flash of Unstyle Content in IE */ ?> 
      //]]>
    </script>
		<style type="text/css">
			.wrapper {width: <?php print theme_get_setting('grass_width'); ?>!important;}
		</style>
  </head>
  <body<?php print $attributes; ?>>
    <?php if (!empty($admin)) print $admin; // support for: http://drupal.org/project/admin ?>
      <div id="header">
       <div class="wrapper">
       <div id="logo">
         <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home">
           <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
         </a>
         <?php if (!empty($site_name)): ?>
           <h1 id="site-name">
             <a href="<?php print $front_page ?>" title="<?php print t('Home'); ?>" rel="home">
             <?php print $site_name; ?>
             </a>
           </h1>
         <?php endif; ?>
       </div><!-- end logo -->
       <div id="nav">
       <?php print $primary_links; ?>
       </div><!-- end nav -->
        </div>
      </div> <!-- end header -->
      <?php if ($feature):  ?>
      <div id="feature">
        <div class="wrapper">
        <?php print $feature; ?>
        </div>
      </div>
      <?php else: ?>
        <div id="breadcrumb">
          <div class="wrapper">
        <?php print $breadcrumb; ?>
          </div>
        </div>
      <?php endif; ?>
   <div class="wrapper">
      <div id="main">
        <?php if (!empty($title)): ?>
          <h1 class="title"><?php print $title; ?></h1>
        <?php endif; ?> <!-- END TITLE -->
         <?php if ($tabs): ?>
            <div id="tabs">
            <?php print $tabs; ?>
            </div> <!-- END TABS -->
          <?php endif; ?>
        <div id="content">
        <?php print $messages; ?>
        <?php print $help; ?>
        <?php print $content; ?>
        </div><!-- end content -->
        <?php if($right): ?>
          <div id="sidebar">
            <?php print $right; ?>
          </div>
        <?php endif; ?>
      </div><!-- end main -->
      <div class="clear"> </div>
      </div><!-- end wrapper -->
      <div id="boxes">   
          <div class="wrapper">
            <?php if ($box_left): ?>
              <div class="box left">
              <?php print $box_left; ?>
              </div>
            <?php endif; ?>
            <?php if ($box_middle): ?>
              <div class="box middle">
              <?php print $box_middle; ?>
              </div>
            <?php endif; ?>
            <?php if ($box_right): ?>
              <div class="box right">
              <?php print $box_right; ?>
              </div>
            <?php endif; ?>
            <div class="clear"> </div>
          </div>
      </div>
      <div id="footer">
        <div class="wrapper">
        <?php if ($footer_content): ?>
          <?php print $footer_content; ?>
        <?php endif; ?>

        <?php print $footer_message; ?>
        </div>
      </div><!--end footer-->
        <?php print $closure; ?>
  </body>
</html>
