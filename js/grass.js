Drupal.behaviors.grassBehaviors = function(context){
  $('fieldset.collapsible .fieldset-title', context).click(hideFieldset);


  function hideFieldset() {
    var $content = $(this).parent().children('.fieldset-body');
    if($content.is(':hidden')){
      $(this).parent().removeClass('collapsed');
      $content.slideDown();
    }else{
      $content.hide();
      $(this).parent().addClass('collapsed');
    }
  }

  sidebarSize();

  function sidebarSize(){
		if($('#left').height() < $('#main').height()){
			var height = $('#main').height();
			$('#left').css('height', height);
		}
	}

 /**
   * Superfish Menus
   * http://users.tpg.com.au/j_birch/plugins/superfish/
   * To use this feature please add the superfish.js to the js directory
   */
  jQuery('#nav ul').superfish({
    animation: { opacity: 'show'},
    easing: 'swing',
    speed: 50,
    autoArrows:  false,
    dropShadows: false /* Needed for IE */
  });
};