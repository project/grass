<?php

/**
 * Implementation of theme_preprocess_HOOK().
 * Passes varables to the node templates.
 *
 * @return $vars
 */

// Grab the node object.
 $node = $vars['node'];
 // Make individual variables for the parts of the date.
 $vars['date_day'] = format_date($node->created, 'custom', 'j');
 $vars['date_month'] = format_date($node->created, 'custom', 'l');
 $vars['date_year'] = format_date($node->created, 'custom', 'M Y');

// Prepare the arrays to handle the classes and ids for the node container.
if(!isset($vars['node']->attributes)) {
  $vars['node_attributes'] = array();
}
else {
  $vars['node_attributes'] = $vars['node']->attributes;
}

// Add an id to allow the styling of a specific node.
$vars['node_attributes']['id'] = 'node-' . $vars['type'] . '-' . $vars['nid'];

// Add a class to allow styling of nodes of a specific type.
$vars['node_attributes']['class'][] = $vars['type'] . '-ntype';

// Add a class to allow styling based on if a node is showing a teaser or the 
// whole thing.
if ($vars['teaser']) {
  $vars['node_attributes']['class'][] = 'teaser-view';
}
else {
  $vars['node_attributes']['class'][] = 'full-view';
}

// Add a class to make the node container self clearing.
$vars['node_attributes']['class'][] = 'clear-block';

// Crunch all the attributes together into a single string to be applied to 
// the node container.
$vars['attributes'] = theme('render_attributes', $vars['node_attributes']);
