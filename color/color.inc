<?php

$info = array(

  // Pre-defined color schemes.
  'schemes' => array(
    '#779964,#779964,#000000,#000000,#333333' => t('Grass (Default)'),
    '#1e7fbe,#1e7fbe,#000000,#000000,#494949' => t('Ash'),
    '#37aabe,#237580,#000000,#111111,#494949' => t('Teal'),
    '#f50066,#d10057,#000000,#000000,#333333' => t('Tokyo Plastic'),
    '#bcc007,#929504,#000000,#000000,#494949' => t('Neon Love'),
    '#928d5d,#5c4b1e,#071c0b,#000000,#494949' => t('Brown'),
    '#aaaaaa,#333333,#111111,#191919,#494949' => t('Grey Wash'),
    '#893ebb,#5b2380,#000000,#111111,#494949' => t('Nocturnal'),
    '#788597,#3f728d,#222222,#111111,#707070' => t('Mercury'),
  ),

  // Images to copy over.
  'copy' => array(
    'images/buttonbg.png',
    'images/tabsbg.png',
		'images/bullet.jpg',
		'logo.png',
  ),

  // CSS files (excluding @import) to rewrite with new color scheme.
  'css' => array(
    'style.css',
  ),

  // Coordinates of gradient (x, y, width, height).
  'gradient' => array(0, 0, 1000, 90),

  // Color areas to fill (x, y, width, height).
  'fill' => array(
    'base' => array(0, 0, 1000, 568),
    'link' => array(107, 533, 41, 23),
  ),

  // Coordinates of all the theme slices (x, y, width, height)
  // with their filename as used in the stylesheet.
  'slices' => array(
    'images/headerbg.png'                => array(0, 0, 2, 90),
    'images/featurebg.png'               => array(0, 91, 1000, 261),
    'images/featurerpt.png'              => array(0, 91, 2, 261),
    'images/breadcrumb.png'              => array(0, 352, 1000, 76),
    'images/breadcrumbrpt.png'           => array(0, 352, 2, 76),


    'screenshot.png'                       => array(0, 0, 400, 240),
  ),

  // Reference color used for blending. Matches the base.png's colors.
  'blend_target' => '#ffffff',

  // Preview files.
  'preview_image' => 'color/preview.png',
  'preview_css' => 'color/preview.css',

  // Base file for image generation.
  'base_image' => 'color/base.png',
);
